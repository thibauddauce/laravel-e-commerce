<?php

namespace Tests\Unit;

use App\Stripe;
use Tests\TestCase;
use App\Exceptions\StripeException;

class StripeTest extends TestCase
{
    const LAST4 = [
        'tok_visa' => '4242',
    ];

    /** @test */
    function it works()
    {
        $last4 = Stripe::charge('tok_visa', 10 * 100);

        $this->assertEquals(self::LAST4['tok_visa'], $last4);
    }

    /** @test */
    function it doesn t works()
    {
        try {
            Stripe::charge('token_invalid', 10 * 100);
        } catch (StripeException $e) {
            $this->assertTrue(true);
            return;
        }

        $this->fail("Stripe succeeded.");
    }
}
