<?php

namespace Tests\Unit;

use App\Stripe;
use App\StripeFake;

class StripeFakeTest extends StripeTest
{
    public function setUp()
    {
        parent::setUp();

        Stripe::fake();
    }
}