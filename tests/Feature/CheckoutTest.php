<?php

namespace Tests\Feature;

use App\Stripe;
use App\Address;
use App\Product;
use App\Checkout;
use App\StripeFake;
use Tests\TestCase;
use Tests\Unit\StripeTest;
use App\ProductWithQuantity;
use App\Mail\CheckoutCompletedMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CheckoutTest extends TestCase
{
    use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();

        Stripe::fake();
        Mail::fake();
    }

    /** @test */
    function it works()
    {
        $this->withoutExceptionHandling();

        $product = factory(Product::class)->create([
            'price_in_cents' => 5 * 100,
        ]);

        $this->post('/cart', [
            'product_id' => $product->id,
            'quantity' => 3,
        ])->assertRedirect();

        $response = $this->post('/checkout', [
            'stripe_token' => 'tok_visa',
            'email' => 'thibaud@example.com',

            'billing_address' => [
                'name' => 'John Doe',
                'line1' => '123 Main Street',
                'line2' => '',
                'line3' => '',
                'postcode' => '42000',
                'city' => 'New York',
                'country' => 'US',
            ],
        ]);

        $response->assertRedirect('/');

        $addresses = Address::all();
        $this->assertCount(1, $addresses);

        $address = $addresses->first();
        $this->assertEquals('John Doe', $address->name);
        $this->assertEquals('123 Main Street', $address->line1);
        $this->assertNull($address->line2);
        $this->assertNull($address->line3);
        $this->assertEquals('42000', $address->postcode);
        $this->assertEquals('New York', $address->city);
        $this->assertEquals('US', $address->country);

        $productsWithQuantities = ProductWithQuantity::all();
        $this->assertCount(1, $productsWithQuantities);

        $productWithQuantity = $productsWithQuantities->first();
        $this->assertTrue($productWithQuantity->product->is($product));
        $this->assertEquals(3, $productWithQuantity->quantity);

        $checkouts = Checkout::all();
        $this->assertCount(1, $checkouts);

        $checkout = $checkouts->first();
        $this->assertEquals(StripeTest::LAST4['tok_visa'], $checkout->card_last4);
        $this->assertTrue($checkout->billingAddress->is($address));
        $this->assertCount(1, $checkout->productsWithQuantities);
        $this->assertTrue($checkout->productsWithQuantities->first()->is($productWithQuantity));

        Stripe::assertCharged('tok_visa', 3 * 5 * 100);

        Mail::assertSent(CheckoutCompletedMail::class, function ($mail) use ($checkout) {
            return $mail->hasTo('thibaud@example.com')
               and $mail->checkout->is($checkout);
        });
    }

    protected function withNonEmptyCart()
    {
        $product = factory(Product::class)->create();

        $this->post('/cart', [
            'product_id' => $product->id,
            'quantity' => 5,
        ])->assertRedirect();

        return $this;
    }

    protected function validParams($overrides = [])
    {
        return array_merge_recursive_distinct([
            'billing_address' => [
                'name' => 'John Doe',
                'line1' => '123 Main Street',
                'line2' => '',
                'line3' => '',
                'postcode' => '42000',
                'city' => 'New York',
                'country' => 'US',
            ],
        ], $overrides);
    }

    /** @test */
    function billing address name is required()
    {
        $this->withNonEmptyCart()
            ->post('/checkout', $this->validParams([
                'stripe_token' => 'tok_visa',

                'billing_address' => [
                    'name' => '',
                ],
            ]))
            ->assertSessionHasErrors('billing_address.name');

        $this->assertEquals(0, Checkout::count());
        Stripe::assertNotCharged('tok_visa');
        Mail::assertNotSent(CheckoutCompletedMail::class);
    }

    /** @test */
    function billing address line 1 is required()
    {
        $this->withNonEmptyCart()
            ->post('/checkout', $this->validParams([
                'billing_address' => [
                    'line1' => '',
                ],
            ]))
            ->assertSessionHasErrors('billing_address.line1');
    }

    /** @test */
    function billing address postcode is required()
    {
        $this->withNonEmptyCart()
            ->post('/checkout', $this->validParams([
                'billing_address' => [
                    'postcode' => '',
                ],
            ]))
            ->assertSessionHasErrors('billing_address.postcode');
    }

    /** @test */
    function billing address city is required()
    {
        $this->withNonEmptyCart()
            ->post('/checkout', $this->validParams([
                'billing_address' => [
                    'city' => '',
                ],
            ]))
            ->assertSessionHasErrors('billing_address.city');
    }

    /** @test */
    function billing address country is required()
    {
        $this->withNonEmptyCart()
            ->post('/checkout', $this->validParams([
                'billing_address' => [
                    'country' => '',
                ],
            ]))
            ->assertSessionHasErrors('billing_address.country');
    }
}
