<?php

namespace Tests\Feature;

use App\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CartTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function show empty cart()
    {
        $this->get('/cart')
            ->assertSuccessful()
            ->assertViewHasEmptyCollection('productsWithQuantities');
    }

    /** @test */
    function add product to cart()
    {
        $product = factory(Product::class)->create([
            'price_in_cents' => 5 * 100,
        ]);

        $this->post('/cart', [
            'product_id' => $product->id,
        ])->assertRedirect();

        $response = $this->get('/cart')
            ->assertSuccessful();

        $productsWithQuantities = $response->getViewData('productsWithQuantities');

        $this->assertCount(1, $productsWithQuantities);
        $this->assertModelEquals($product, $productsWithQuantities->first()->product);
        $this->assertEquals(1, $productsWithQuantities->first()->quantity);
        $this->assertEquals(5 * 100, $productsWithQuantities->totalPriceInCents());
    }

    /** @test */
    function add non existing product should fail()
    {
        $this->post('/cart', [
            'product_id' => 999,
        ])->assertStatus(404);

        $this->get('/cart')
            ->assertSuccessful()
            ->assertViewHasEmptyCollection('productsWithQuantities');
    }

    /** @test */
    function add product with not a number quantity should fail()
    {
        $product = factory(Product::class)->create();

        $this->post('/cart', [
            'product_id' => $product->id,
            'quantity' => 'not_a_number',
        ])->assertSessionHasErrors('quantity');

        $this->get('/cart')
            ->assertSuccessful()
            ->assertViewHasEmptyCollection('productsWithQuantities');
    }

    /** @test */
    function add product with negative quantity should fail()
    {
        $product = factory(Product::class)->create();

        $this->post('/cart', [
            'product_id' => $product->id,
            'quantity' => -42,
        ])->assertSessionHasErrors('quantity');
        $this->post('/cart', [
            'product_id' => $product->id,
            'quantity' => 0,
        ])->assertSessionHasErrors('quantity');

        $this->get('/cart')
            ->assertSuccessful()
            ->assertViewHasEmptyCollection('productsWithQuantities');
    }

    /** @test */
    function can add two products()
    {
        $productA = factory(Product::class)->create([
            'price_in_cents' => 5 * 100,
        ]);
        $productB = factory(Product::class)->create([
            'price_in_cents' => 7 * 100,
        ]);

        $this->post('/cart', [
            'product_id' => $productA->id,
        ])->assertRedirect();
        $this->post('/cart', [
            'product_id' => $productB->id,
        ])->assertRedirect();

        $response = $this->get('/cart')
            ->assertSuccessful();

        $productsWithQuantities = $response->getViewData('productsWithQuantities');

        $this->assertCount(2, $productsWithQuantities);
        $this->assertModelEquals($productA, $productsWithQuantities[$productA->id]->product);
        $this->assertEquals(1, $productsWithQuantities[$productA->id]->quantity);
        $this->assertModelEquals($productB, $productsWithQuantities[$productB->id]->product);
        $this->assertEquals(1, $productsWithQuantities[$productB->id]->quantity);
        $this->assertEquals((5 + 7) * 100, $productsWithQuantities->totalPriceInCents());
    }

    /** @test */
    function can add a product twice()
    {
        $product = factory(Product::class)->create([
            'price_in_cents' => 5 * 100,
        ]);

        $this->post('/cart', [
            'product_id' => $product->id,
        ])->assertRedirect();
        $this->post('/cart', [
            'product_id' => $product->id,
        ])->assertRedirect();

        $response = $this->get('/cart')
            ->assertSuccessful();

        $productsWithQuantities = $response->getViewData('productsWithQuantities');

        $this->assertCount(1, $productsWithQuantities);
        $this->assertModelEquals($product, $productsWithQuantities->first()->product);
        $this->assertEquals(2, $productsWithQuantities->first()->quantity);
        $this->assertEquals(2 * 5 * 100, $productsWithQuantities->totalPriceInCents());
    }

    /** @test */
    function can add a product with quantity()
    {
        $product = factory(Product::class)->create();

        $this->post('/cart', [
            'product_id' => $product->id,
            'quantity' => 3,
        ])->assertRedirect();

        $response = $this->get('/cart')
            ->assertSuccessful();

        $productsWithQuantities = $response->getViewData('productsWithQuantities');

        $this->assertCount(1, $productsWithQuantities);
        $this->assertModelEquals($product, $productsWithQuantities->first()->product);
        $this->assertEquals(3, $productsWithQuantities->first()->quantity);

        $this->post('/cart', [
            'product_id' => $product->id,
            'quantity' => 8,
        ])->assertRedirect();

        $response = $this->get('/cart')
            ->assertSuccessful();

        $productsWithQuantities = $response->getViewData('productsWithQuantities');

        $this->assertCount(1, $productsWithQuantities);
        $this->assertModelEquals($product, $productsWithQuantities->first()->product);
        $this->assertEquals(3 + 8, $productsWithQuantities->first()->quantity);
    }

    /** @test */
    function can modify a product with quantity()
    {
        $productA = factory(Product::class)->create();
        $productB = factory(Product::class)->create();

        $this->post('/cart', [
            'product_id' => $productA->id,
            'quantity' => 3,
        ])->assertRedirect();
        $this->post('/cart', [
            'product_id' => $productB->id,
            'quantity' => 6,
        ])->assertRedirect();
        $this->patch('/cart', [
            'product_id' => $productA->id,
            'quantity' => 5,
        ])->assertRedirect();

        $response = $this->get('/cart');

        $productsWithQuantities = $response->getViewData('productsWithQuantities');

        $this->assertCount(2, $productsWithQuantities);
        $this->assertModelEquals($productA, $productsWithQuantities[$productA->id]->product);
        $this->assertEquals(5, $productsWithQuantities[$productA->id]->quantity);
        $this->assertModelEquals($productB, $productsWithQuantities[$productB->id]->product);
        $this->assertEquals(6, $productsWithQuantities[$productB->id]->quantity);
    }

    /** @test */
    function cannot modify a product without quantity()
    {
        $product = factory(Product::class)->create();

        $this->patch('/cart', [
            'product_id' => $product->id,
        ])->assertSessionHasErrors('quantity');

        $response = $this->get('/cart')
            ->assertSuccessful()
            ->assertViewHasEmptyCollection('productsWithQuantities');
    }

    /** @test */
    function cannot modify a product with not a number quantity()
    {
        $product = factory(Product::class)->create();

        $this->patch('/cart', [
            'product_id' => $product->id,
            'quantity' => 'not_a_number'
        ])->assertSessionHasErrors('quantity');

        $response = $this->get('/cart')
            ->assertSuccessful()
            ->assertViewHasEmptyCollection('productsWithQuantities');
    }

    /** @test */
    function cannot modify a non existing product()
    {
        $this->patch('/cart', [
            'product_id' => 999,
            'quantity' => 4,
        ])->assertStatus(404);
    }

    /** @test */
    function cannot modify a product with negative quantity()
    {
        $product = factory(Product::class)->create();

        $this->patch('/cart', [
            'product_id' => $product->id,
            'quantity' => -42,
        ])->assertSessionHasErrors('quantity');
        $this->patch('/cart', [
            'product_id' => $product->id,
            'quantity' => 0,
        ])->assertSessionHasErrors('quantity');

        $response = $this->get('/cart')
            ->assertSuccessful()
            ->assertViewHasEmptyCollection('productsWithQuantities');
    }

    /** @test */
    function can delete a product()
    {
        $productA = factory(Product::class)->create();
        $productB = factory(Product::class)->create();

        $this->post('/cart', [
            'product_id' => $productA->id,
            'quantity' => 3,
        ])->assertRedirect();
        $this->post('/cart', [
            'product_id' => $productB->id,
            'quantity' => 6,
        ])->assertRedirect();
        $this->delete('/cart', [
            'product_id' => $productA->id,
        ])->assertRedirect();

        $response = $this->get('/cart');

        $productsWithQuantities = $response->getViewData('productsWithQuantities');

        $this->assertCount(1, $productsWithQuantities);
        $this->assertModelEquals($productB, $productsWithQuantities->first()->product);
        $this->assertEquals(6, $productsWithQuantities->first()->quantity);
    }
}
