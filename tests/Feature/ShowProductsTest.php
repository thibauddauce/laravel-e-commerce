<?php

namespace Tests\Feature;

use App\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShowProductsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it works()
    {
        $this->withoutExceptionHandling();

        $products = factory(Product::class, 3)->create();

        $this->get('/')
            ->assertSuccessful()
            ->assertViewIs('products.index')
            ->assertViewHas('products', function ($viewProducts) use ($products) {
                return $viewProducts[0]->is($products[0])
                   and $viewProducts[1]->is($products[1])
                   and $viewProducts[2]->is($products[2]);
            });
    }
}
