<?php

namespace Tests;

use PHPUnit\Framework\Assert;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Collection;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function setUp()
    {
        parent::setUp();

        TestResponse::macro('assertViewHasEmptyCollection', function ($key) {
            $collection = $this->getViewData($key);
            Assert::assertInstanceOf(Collection::class, $collection);
            Assert::assertEmpty($collection);
        });

        TestResponse::macro('getViewData', function ($key) {
            $this->assertViewHas($key);
            return $this->original->getData()[$key];
        });
    }

    protected function assertModelEquals($expected, $actual)
    {
        $this->assertInstanceOf(Model::class, $expected);
        $this->assertInstanceOf(Model::class, $actual);
        $this->assertEquals(get_class($expected), get_class($actual));
        $this->assertTrue(
            $expected->is($actual),
            "Failed asserting model #{$actual->id} is #{$expected->id}"
        );
    }
}
