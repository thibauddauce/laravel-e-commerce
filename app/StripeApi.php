<?php

namespace App;

use Stripe\Error\InvalidRequest;
use App\Exceptions\StripeException;

class StripeApi
{
    public static function fake()
    {
        return app()->instance(self::class, new StripeFake);
    }

    public function charge($token, $amount)
    {
        \Stripe\Stripe::setApiKey("sk_test_4eC39HqLyjWDarjtT1zdp7dc");

        try {
            $charge = \Stripe\Charge::create([
                "amount" => $amount,
                "currency" => "eur",
                "source" => $token, // obtained with Stripe.js
            ]);

            return $charge->source->last4;
        } catch (InvalidRequest $e) {
            throw new StripeException;
        }
    }
}
