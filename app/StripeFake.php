<?php

namespace App;

use App\Exceptions\StripeException;
use PHPUnit\Framework\Assert as PHPUnit;

class StripeFake
{
    public $charges;

    public function __construct()
    {
        $this->charges = collect();
    }

    public function charge($token, $amount)
    {
        if ($token === 'tok_visa') {
            $this->charges[$token] = $this->getChargesFor($token)->push($amount);
            return '4242';
        }

        throw new StripeException;
    }

    public function assertCharged($token, $amount)
    {
        PHPUnit::assertTrue($this->charges->has($token));
        PHPUnit::assertCount(1, $charges = $this->getChargesFor($token));
        PHPUnit::assertEquals($amount, $charges->first());
    }

    public function assertNotCharged($token)
    {
        PHPUnit::assertFalse($this->charges->has($token));
    }

    public function getChargesFor($token)
    {
        return $this->charges->get($token, collect());
    }
}
