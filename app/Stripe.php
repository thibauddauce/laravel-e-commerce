<?php

namespace App;

use Illuminate\Support\Facades\Facade;

class Stripe extends Facade
{
    /**
     * Replace the bound instance with a fake.
     *
     * @return void
     */
    public static function fake()
    {
        static::swap(new StripeFake);
    }

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return StripeApi::class;
    }
}