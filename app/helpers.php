<?php

function array_undot($arrayDot)
{
    $array = [];

    foreach ($arrayDot as $key => $value) {
        array_set($array, $key, $value);
    }

    return $array;
}

function array_merge_recursive_distinct($array1, $array2)
{
    $array1Dot = array_dot($array1);
    $array2Dot = array_dot($array2);

    $arrayMergeDot = array_merge($array1Dot, $array2Dot);

    return array_undot($arrayMergeDot);
}
