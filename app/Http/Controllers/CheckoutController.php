<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Stripe;
use App\Address;
use App\Checkout;
use App\Mail\CheckoutCompletedMail;
use Illuminate\Support\Facades\Mail;

class CheckoutController extends Controller
{
    public function store()
    {
        request()->validate([
            'billing_address.name' => ['required'],
            'billing_address.line1' => ['required'],
            'billing_address.postcode' => ['required'],
            'billing_address.city' => ['required'],
            'billing_address.country' => ['required'],
        ]);

        $address = Address::create([
            'name' => request('billing_address.name'),
            'line1' => request('billing_address.line1'),
            'line2' => request('billing_address.line2'),
            'line3' => request('billing_address.line3'),
            'postcode' => request('billing_address.postcode'),
            'city' => request('billing_address.city'),
            'country' => request('billing_address.country'),
        ]);

        $cart = Cart::fromSession();

        $cardLast4 = Stripe::charge(request('stripe_token'), $cart->totalPriceInCents());

        $checkout = Checkout::create([
            'card_last4' => $cardLast4,
            'billing_address_id' => $address->id,
        ]);

        $cart->each(function ($productWithQuantity) use ($checkout) {
            $checkout->productsWithQuantities()->save($productWithQuantity);
        });

        Mail::to(request('email'))->send(new CheckoutCompletedMail($checkout));

        return redirect('/');
    }
}
